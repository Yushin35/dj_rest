from django.conf.urls import url, include

from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_nested import routers

from kitchen.views import (
    IngredientViewSet,
    DishViewSet,
    DishAmountViewSet,
)


router = routers.SimpleRouter()

router.register(r'dishes', DishViewSet, basename='dishes')
router.register(r'ingredients', IngredientViewSet, basename='ingredients')

dish_router = routers.NestedSimpleRouter(router, r'dishes', lookup='dish')
dish_router.register(r'q', DishAmountViewSet, basename='quantities')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(dish_router.urls)),
    url(r'^api-token-auth/', obtain_jwt_token),
]
