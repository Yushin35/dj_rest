from rest_framework.exceptions import APIException


class InvalidData(APIException):
    status_code = 400
    default_detail = 'Your request contain invalid data'
    default_code = 'Invalid data'


class ResourceAlreadyExist(APIException):
    status_code = 409
    default_detail = 'Resource has been created'
    default_code = 'Resource has been created'
