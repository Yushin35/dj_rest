from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.


class User(AbstractUser):
    pass


class Dish(models.Model):
    name = models.CharField(max_length=255)
    owner = models.ForeignKey(User, related_name='dishes', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    name = models.CharField(max_length=255)
    dishes = models.ManyToManyField(Dish, through='Quantity')

    def __str__(self):
        return self.name


class Quantity(models.Model):
    DEFAULT_MEASUREMENT = 'gram'

    amount = models.IntegerField()
    measurement = models.CharField(
        choices=(
            ('gram', 'gram'),
            ('liter', 'liter'),
        ),
        default=DEFAULT_MEASUREMENT,
        max_length=50,
    )
    ingredient = models.ForeignKey(Ingredient, related_name='+', on_delete=models.CASCADE)
    dish = models.ForeignKey(Dish, related_name='ingredient_quantities', on_delete=models.CASCADE)

    class Meta:
        unique_together = ('dish', 'ingredient')
