from django.db import IntegrityError

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, IntegerField, RelatedField

from kitchen.models import (
    Dish,
    Quantity,
    Ingredient,
    User,
)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class IngredientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingredient
        fields = ('id', 'name')


class QuantitySerializer(serializers.ModelSerializer):
    ingredient = IngredientSerializer(read_only=True, required=False)

    class Meta:
        model = Quantity
        fields = ('id', 'amount', 'measurement', 'ingredient')


class DetailedDishSerializer(serializers.ModelSerializer):
    ingredient_quantities = QuantitySerializer(many=True, required=False)
    owner = UserSerializer(required=False, read_only=True)

    class Meta:
        model = Dish
        fields = ('id', 'name', 'owner', 'ingredient_quantities')


class SimpleDishSerializer(serializers.ModelSerializer):
    owner = UserSerializer(required=False, read_only=True)

    class Meta:
        model = Dish
        fields = ('id', 'name', 'owner')
