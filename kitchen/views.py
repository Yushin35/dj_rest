from django.db import IntegrityError

from rest_framework import viewsets
from rest_framework.exceptions import NotFound
from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated

from kitchen.permissions import IsAdminOrOwnerOrReadOnly, IsAdminOrReadOnly
from kitchen.models import Dish, Quantity, Ingredient
from kitchen.serializers import (
    DetailedDishSerializer,
    SimpleDishSerializer,
    QuantitySerializer,
    IngredientSerializer,
)


class IngredientViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAdminOrReadOnly, IsAuthenticated)

    serializer_class = IngredientSerializer
    queryset = Ingredient.objects.all()


class DishViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAdminOrOwnerOrReadOnly, IsAuthenticated)

    queryset = Dish.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return DetailedDishSerializer
        return SimpleDishSerializer


class DishAmountViewSet(mixins.UpdateModelMixin,
                        mixins.DestroyModelMixin,
                        viewsets.GenericViewSet):
    serializer_class = QuantitySerializer
    lookup_field = 'ingredient_pk'

    def get_object(self):
        try:
            quantity, _ = Quantity.objects.get_or_create(
                dish_id=self.kwargs['dish_pk'],
                ingredient_id=self.kwargs['ingredient_pk'],
            )
        except IntegrityError:
            raise NotFound

        return quantity
